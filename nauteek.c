/*

    Format converter module skeleton.

    Steps to create a new format.

    4) Create a new section in vecs.c.
    5) Add compilation instructions to Makefile.
    6) Add sample files (it's better when they're created by the "real" 
       application and not our own output) to reference/ along with 
       files in a well supported (preferably non-binary) format and 
       entries in our 'testo' program.   This allows users of different
       OSes and hardware to exercise your module.

    Copyright (C) 2011 Norbert Kiesel, nk@iname.com
    Copyright (C) 2005  Robert Lipe, robertlipe@usa.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111 USA

 */

#include "defs.h"
#include <ctype.h>

#define MYNAME "nauteek"

#define PAGESIZE 1056
#define MAX_WAYPOINTS 759 // 924
#define WP_NAME_LENGTH 8
#define RT_NAME_LENGTH 8

typedef	struct {
      char name[WP_NAME_LENGTH];
      float latitude;
      float longitude;
} nk_waypoint;

static gbfile *fin;
static gbfile *fout;

// global array containing waypoints
static waypoint * waypoints[MAX_WAYPOINTS];
static int num_waypoints;

static gbuint16 routeoffsets[PAGESIZE/3];
static int num_routes;
static gbuint16 current_rte_offset;


// Any arg in this list will appear in command line help and will be 
// populated for you.
// Values for ARGTYPE_xxx can be found in defs.h and are used to 
// select the type of option.
static
arglist_t nauteek_args[] = {
// {"foo", &fooopt, "The text of the foo option in help", 
//   "default", ARGYTPE_STRING, ARG_NOMINMAX} , 
	ARG_TERMINATOR
};

/*******************************************************************************
* %%%        global callbacks called by gpsbabel main process              %%% *
*******************************************************************************/

static float rad2deg(float rad)
{
  return isnan(rad) ? rad : 180 * rad / M_PI;
}

static float deg2rad(float deg)
{
  return isnan(deg) ? deg : deg * M_PI / 180;
}

static void
nk_collect_waypt(const waypoint *wp)
{
	waypoints[num_waypoints++] = (waypoint *) wp;
}

static void
nk_write_rte_offset(const route_head *rh)
{
	gbfputc(1, fout);
	gbfputuint16(current_rte_offset, fout);
	if (rh) 
	{
		current_rte_offset += RT_NAME_LENGTH + sizeof(gbuint16) * rh->rte_waypt_ct;
	}
}


static void
nk_write_rte_name(const route_head *rh)
{
	gbfprintf(fout, "%-*.*s", RT_NAME_LENGTH, RT_NAME_LENGTH, rh->rte_name);
}

static void
nk_write_waypoints() 
{
	int idx;
	for (idx = 0; idx < MAX_WAYPOINTS; idx++) {
		nk_waypoint nwp;
		if (idx < num_waypoints) {
			waypoint * wp = waypoints[idx];
			snprintf(nwp.name, WP_NAME_LENGTH, "%-*s", WP_NAME_LENGTH, wp->shortname);
			nwp.latitude = deg2rad(wp->latitude);
			nwp.longitude = deg2rad(wp->longitude);
		} else {
			memset(&nwp, '\377', sizeof(nwp));
		}
		is_fatal(gbfwrite(&nwp, sizeof(nwp), 1, fout) != 1, "error writing waypoint %u\n", idx);
	}
}


static void
nk_write_rte_waypoint(const waypoint *wp)
{
	int idx;
	char kind = 0;
	char * wpname = xstrdup(wp->shortname);
	rtrim(wpname);
	if (str_match(wpname, "* [?]")) {
		switch (wpname[strlen(wpname) - 2]) {
			case 'P':
				kind = 0;
				break;
			case 'S':
				kind = 8;
				break;
			case 'G':
				kind = 12;
				break;
			default:
				warning("Unsupported route waypoint kind %c\n", kind);
				kind = 0;
		}
		wpname[strlen(wpname) - 4] = '\0';
	}

	for (idx = 0; idx < num_waypoints; idx++) {
		if (strcmp(waypoints[idx]->shortname, wpname) == 0) {
			gbfputuint16((kind << 12) | (idx & 0xfff), fout);
			break;
		}
	}
	if (idx == num_waypoints) {
		warning("Missing route waypoint %s\n", wpname);
	}
	xfree(wpname);
}

static void
nk_read_waypoints()
{
	for (num_waypoints = 0; num_waypoints < MAX_WAYPOINTS; num_waypoints++) {
		nk_waypoint nwp;
		waypoint * wp;
		if (gbfread(&nwp, sizeof(nwp), 1, fin) != 1)
			break;
		wp = waypt_new();
		wp->shortname = xstrndup(nwp.name, WP_NAME_LENGTH);
		rtrim(wp->shortname);
		wp->latitude = rad2deg(nwp.latitude);
		wp->longitude = rad2deg(nwp.longitude);
		if (isnan(wp->latitude) || isnan(wp->longitude))
			break;
		waypt_add(wp);
		/* We need the waypoints again for the routes */
		waypoints[num_waypoints] = wp;
	}
	num_waypoints++;
}

static void
nk_read_rte_offsets()
{
	for (num_routes = 0; num_routes < PAGESIZE/3; num_routes++) {
		if (gbfgetc(fin) != 1)
			break;
		routeoffsets[num_routes] = gbfgetuint16(fin);
	}
}

static void
nk_read_routes()
{
	int r;
	for (r = 0; r < num_routes - 1; r++) {
		gbuint16 rs = routeoffsets[r];
		gbuint16 re = routeoffsets[r + 1];
		gbuint16 numwp = (re - rs - RT_NAME_LENGTH) / 2;
		char buf[RT_NAME_LENGTH + numwp * sizeof(gbuint16) ];
		int w;
		route_head * rte_head;
		if (gbfread(buf, sizeof(buf), 1, fin) != 1)
			break;
		rte_head = route_head_alloc();
		rte_head->rte_name = xstrndup(buf, RT_NAME_LENGTH);
		rtrim(rte_head->rte_name);
		route_add_head(rte_head);
		for (w = 0; w < numwp; w++) {
			gbuint16 wpd = le_read16(buf + RT_NAME_LENGTH + w * sizeof(gbuint16));
			gbuint16 idx = wpd & 0xfff;
			if (idx >= num_waypoints) {
				warning("Waypoint index %u out of range\n", idx);
				continue;
			}
			waypoint * wp = waypt_dupe(waypoints[idx]);
			switch (wpd >> 12) {
				case 0:
					xstrappend(wp->shortname, " [P]");
					xstrappend(wp->description, " [port rounding]");
					break;
				case 8:
					xstrappend(wp->shortname, " [S]");
					xstrappend(wp->description, " [starboard rounding]");
					break;
				case 12:
					xstrappend(wp->shortname, " [G]");
					xstrappend(wp->description, " [gate]");
					break;
			}
			route_add_wpt(rte_head, wp);
		}
	}
}

static void
nauteek_rd_init(const char *fname)
{
	fin = gbfopen(fname, "r", MYNAME);
}

static void 
nauteek_rd_deinit(void)
{
	gbfclose(fin);
}

static void
nauteek_read(void)
{
	gbfseek(fin, PAGESIZE * 5, SEEK_SET);
	nk_read_waypoints();
	
	gbfseek(fin, 0, SEEK_SET);
	memset(routeoffsets, '\0', sizeof(routeoffsets));
	nk_read_rte_offsets();
	
	gbfseek(fin, PAGESIZE, SEEK_SET);
	nk_read_routes();
}

static void
nauteek_wr_init(const char *fname)
{
	int i;
	fout = gbfopen(fname, "w", MYNAME);
	for (i = 0; i < PAGESIZE * 5; i++) {
		gbfputc('\377', fout);
	}
}

static void
nauteek_wr_deinit(void)
{
	gbfclose(fout);
}

static void
nauteek_write(void)
{
	num_waypoints = 0;
	current_rte_offset = 0;
	
	// read all waypoints
	waypt_disp_all(nk_collect_waypt);

	if (global_opts.objective == rtedata) {
		// write route offsets
		gbfseek(fout, 0, SEEK_SET);
		route_disp_all(nk_write_rte_offset, NULL, NULL);
		nk_write_rte_offset(NULL);

		// write routes
		gbfseek(fout, PAGESIZE, SEEK_SET);
		route_disp_all(nk_write_rte_name, NULL, nk_write_rte_waypoint);
	}
	
	// always write waypoints because routes depend on them
	gbfseek(fout, PAGESIZE * 5, SEEK_SET);
	nk_write_waypoints();
}

static void
nauteek_exit(void)		/* optional */
{
}

/**************************************************************************/

// capabilities below means: we can only read and write waypoints
// please change this depending on your new module 

ff_vecs_t nauteek_vecs = {
	ff_type_file,
	{ 
		ff_cap_read | ff_cap_write  	/* waypoints */, 
	  	ff_cap_none 			/* tracks */, 
	  	ff_cap_read | ff_cap_write	/* routes */
	},
	nauteek_rd_init,	
	nauteek_wr_init,	
	nauteek_rd_deinit,	
	nauteek_wr_deinit,	
	nauteek_read,
	nauteek_write,
	nauteek_exit,
	nauteek_args,
	CET_CHARSET_ASCII, 0			/* ascii is the expected character set */
						/* not fixed, can be changed through command line parameter */
};
/**************************************************************************/
